QT += core
QT -= gui

TARGET = fluxspeed-ng
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    hash.cpp

HEADERS += \
    hash.hpp

QMAKE_CXXFLAGS += -std=c++1y

unix:!macx: LIBS += -L$$PWD/../../../usr/local/lib/ -lboost_system

INCLUDEPATH += $$PWD/../../../usr/local/include
DEPENDPATH += $$PWD/../../../usr/local/include

unix:!macx: LIBS += -L$$PWD/../../../usr/local/lib/ -lboost_thread

INCLUDEPATH += $$PWD/../../../usr/local/include
DEPENDPATH += $$PWD/../../../usr/local/include

DISTFILES += \
    README.md \
    LICENSE
