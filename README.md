fluxspeed-ng
============

Test how quickly your computer can hash fluxhash. Expect a low value ~300 key/s.

BUILDING
--------

Build with g++, Boost.System and Boost.Thread libraries:

g++ -o fluxspeed-ng main.cpp hash.cpp -lboost_system -lboost_thread -std=c++1y
