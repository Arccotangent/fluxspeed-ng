#include <iostream>
#include <ctime>
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>
#include <unistd.h>
#include <random>
#include "hash.hpp"
using namespace std;
time_t initial = time(0);
unsigned int count_int = 0;
string flux = "";
string pw = "";

string fluxsplitter()
{
	string dd_tmp = "";
	for (unsigned short i = 0; i < flux.length() / 2; i++)
	{
		if (i % 16 == 0)
			dd_tmp = dd_tmp + "\n" + flux.at(i) + flux.at(i + 1);
		else
			dd_tmp = dd_tmp + " " + flux.at(i) + flux.at(i + 1);
	}
	boost::to_upper(dd_tmp);
	return dd_tmp;
}

void counter()
{
	time_t current = time(0);
	unsigned int diff = current - initial;
	unsigned int hps = count_int / diff;
	//system("clear");
	cout << "\033[1;1H";
	cout << count_int << " passwords tested (" << hps << " key/s)" << endl;
	cout << endl;
	cout << "Current password: " << pw << endl;
	cout << endl;
	flux = fluxhash(pw);
	cout << "FINAL HASH: " << fluxsplitter() << flush;
}

void dsp()
{
	usleep(1800000);
	while (true)
	{
		boost::thread helium(counter);
		helium.join();
		usleep(200000);
	}
}

void speedtest()
{
	system("clear");
	boost::thread hydrogen(dsp);
	//hydrogen.join();
	random_device r;
	while (true)
	{
		pw = to_string(r());
		flux = fluxhash(pw);
		count_int++;
		//cout << "Hashes calculated: " << count << "\r" << flush;
	}
}

int main()
{
	fprintf(stdout, "Speedtest beginning...\n");
	speedtest();
}
